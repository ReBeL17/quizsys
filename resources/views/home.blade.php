@extends('layouts.admin')
@section('content')
<div class="content">
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>{{$total_categories}}</h3>

              <p>Categories</p>
            </div>
            <div class="icon">
              <i class="fas fa-gem"></i>
            </div>
            <a href="{{route('admin.categories.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>{{$total_questions}}</h3>

              <p>Questions</p>
            </div>
            <div class="icon">
                <i class="ion ion-help"></i>
            </div>
            <a href="{{route('admin.questions.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3>{{$total_users}}</h3>

              <p>User Registrations</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{route('admin.users.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>{{$total_games}}</h3>

              <p>Games Played</p>
            </div>
            <div class="icon">
              <i class="fab fa-playstation"></i>
            </div>
            <a href="{{route('admin.results.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

    <div class="row">
        <div class="col-lg-12">
            
            <div class="banner-item-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Bar chart -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <div class="box-body">
                                    <div style=""></div>
                                    <div id="bar"
                                        style="min-width: 310px; min-height: 400px; max-width: 1500px; max-height: 800px; margin: 0 auto"></div>
                                </div>
                            </div>
                            <!-- /.box-body-->
                        </div>
                        <!-- /.box -->
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
{{-- <script src="https://code.highcharts.com/modules/drilldown.js"></script> --}}

<script>
// Create the bar chart
            Highcharts.chart('bar', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Top Scorers'
                },            
                accessibility: {
                    announceNewData: {
                        enabled: true
                    }
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'points'
                    }
        
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },
        
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y} </b>points <br/>'
                },
        
                series: [
                    {
                        name: "Scores",
                        colorByPoint: true,
                        data: [ 
                                @foreach($results as $result) 
                                {name: "{{$result->user->name}}", y: {{$result->total}} },
                                @endforeach     
                             ]
                    }
                ],
                
            });
</script>
@endsection