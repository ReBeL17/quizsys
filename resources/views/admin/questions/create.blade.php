@extends('layouts.admin')

@section('styles')
 <!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('css/bootstrap/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/admin/addQuizform.css') }}">   
<style>
    .mcq, .add_mcq_option, .maq, .add_maq_option {
        display: none;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.question.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.questions.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="field-wrapper">
                <label for="category_id" placeholder="Please select a category">Please select a category</label>
                <select class="{{ $errors->has('category') ? 'is-invalid' : '' }}" name="category_id" id="category_id" required> 
                    @foreach($categories as $id => $category)
                        <option value="{{ $id }}" {{ old('category_id') == $id ? 'selected' : '' }}>{{ $category }}</option>
                    @endforeach
                </select>
                @if($errors->has('category_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('category_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.category_helper') }}</span>
            </div>
            <div class="field-wrapper">
                <label for="subcategory_id" placeholder="Please select a subcategory">Please select a subcategory</label>
                <select class="{{ $errors->has('subcategory') ? 'is-invalid' : '' }}" name="subcategory_id" id="subcategory_id" required> 
                    {{-- @foreach($categories as $id => $category) --}}
                        <option value="">Please select a subcategory</option>
                    {{-- @endforeach --}}
                </select>
                @if($errors->has('subcategory_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('subcategory_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.subcategory_helper') }}</span>
            </div>
            <div class="field-wrapper">
                <label for="type" placeholder="Please select a type">Please select a type</label>
                <select class="{{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type" required> 
                    <option value="" >Please select type</option>
                        <option value="Multiple Choices" {{ old('type') == 'Multiple Choices' ? 'selected' : '' }}>Multiple Choices</option>
                        <option value="Multiple Answers" {{ old('type') == 'Multiple Answers' ? 'selected' : '' }}>Multiple Answers</option>
                        <option value="True or False " {{ old('type') == 'True or False ' ? 'selected' : '' }}>True or False </option>
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                {{-- <span class="help-block">{{ trans('cruds.question.fields.type_helper') }}</span> --}}
            </div>
            <div class="form-group" id="quest">
                <textarea class="{{ $errors->has('question_text') ? 'is-invalid' : '' }}" name="question_text" id="question_text" required>{{ old('question_text') }}</textarea>
                <label for="question_text" placeholder="Write your question here" alt="question"></label>
                @if($errors->has('question_text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('question_text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.question_text_helper') }}</span>
            </div>

        <div class="mcq" id="mcq">
            <div class="col-md-12 row">
                <div class="icheck-success">
                    <input type="radio" name="points" id="option1" value="1" >
                    <label for="option1">
                    </label>
                </div>
                <div class="col-md-8">
                <input class="{{ $errors->has('option_text') ? 'is-invalid' : '' }}" name="option_text[]" id="option_text" type="text" value="{{ old('option_text[]', '') }}" >
                
                <label for="option_text" placeholder="Option 1" alt="option"></label>
                </div>
                @if($errors->has('option_text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('option_text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span>
            </div>

            <div class="col-md-12 row">
                <div class="icheck-success">
                    <input type="radio" name="points" id="option2" value="2" >
                    <label for="option2">
                    </label>
                </div>
                <div class="col-md-8">
                <input class="{{ $errors->has('option_text') ? 'is-invalid' : '' }}" name="option_text[]" id="option_text" type="text" value="{{ old('option_text[]', '') }}" >
                <label for="option_text" placeholder="Option 2" alt="option"></label>
                </div>
                @if($errors->has('option_text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('option_text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span>
            </div>
        </div>
        <div class="form-group add_mcq_option" id="add_mcq_option">
           <input type="button" class="btn btn-default" id="add" value="Add more option"> 
        </div>

        {{-- multi answers --}}
        <div class="maq" id="maq">
            <div class="col-md-12 row">
                <div class="icheck-success">
                    <input type="checkbox" name="maq_points[]" id="checkbox1" value="1" >
                    <label for="checkbox1">
                    </label>
                </div>
                <div class="col-md-8">
                <input class="option_text2 {{ $errors->has('option_text2') ? 'is-invalid' : '' }}" name="option_text2[]" id="option_text2" type="text" value="{{ old('option_text2[]', '') }}" >
                
                <label for="option_text2" placeholder="Option 1" alt="option"></label>
                </div>
                @if($errors->has('option_text2'))
                    <div class="invalid-feedback">
                        {{ $errors->first('option_text2') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span>
            </div>

            <div class="col-md-12 row">
                <div class="icheck-success">
                    <input type="checkbox" name="maq_points[]" id="checkbox2" value="2">
                    <label for="checkbox2">
                    </label>
                </div>
                <div class="col-md-8">
                <input class="option_text2 {{ $errors->has('option_text2') ? 'is-invalid' : '' }}" name="option_text2[]" id="option_text2" type="text" value="{{ old('option_text2[]', '') }}" >
                <label for="option_text2" placeholder="Option 2" alt="option"></label>
                </div>
                @if($errors->has('option_text2'))
                    <div class="invalid-feedback">
                        {{ $errors->first('option_text2') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span>
            </div>
        </div>
        <div class="form-group add_maq_option" id="add_maq_option">
           <input type="button" class="btn btn-default" id="add" value="Add more option"> 
        </div>

        {{-- <div class="form-group">
            <textarea class="{{ $errors->has('answer_explanation') ? 'is-invalid' : '' }}" name="answer_explanation" id="answer_explanation" required>{{ old('answer_explanation') }}</textarea>
            <label for="answer_explanation" placeholder="Write explanation of correct answer here" alt="answer_explanation"></label>
            @if($errors->has('answer_explanation'))
                <div class="invalid-feedback">
                    {{ $errors->first('answer_explanation') }}
                </div>
            @endif
            <span class="help-block">{{ trans('cruds.question.fields.answer_explanation_helper') }}</span>
        </div> --}}
    </div>
        <div class="card-footer">
                <div class="form-group">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
        </div>
        </form>
    
</div>
@endsection

@section('scripts')
   <script src="{{ asset('js/admin/adaptiveDropdown.js') }}"></script>
   <script type="text/javascript">

        $(document).ready(function(){
            
                var i=$('#mcq input[type="radio"]').length;
            //adding options dynamically
            $('#add_mcq_option').click(function(){
                i++;
                $('#mcq').append('<div class="col-md-12 row" id="row'+i+'"><div class="icheck-success"><input type="radio" name="points" id="option'+i+'" value="'+i+'"><label for="option'+i+'"></label></div><div class="col-md-8"><input class="{{ $errors->has('option_text') ? 'is-invalid' : '' }}" name="option_text['+(i-1)+']" id="option_text" type="text" value="{{ old('option_text[]', '') }}" required><label for="option_text" placeholder="New Option" alt="option"></label></div><div class="col-md-1"><button id="'+i+'" class="btn btn-secondary remove" alt="Delete this option"><i class="fas fa-trash"></i></button></div>@if($errors->has('option_text'))<div class="invalid-feedback">{{ $errors->first('option_text') }}</div>@endif<span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span></div>');
                               
                var j = $('#mcq input[type="radio"]').length;
                // console.log(j)
                if(j>=4){
                        $('#add_mcq_option').hide();
                    }
            });
            
            $('body').on('click','.remove',function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                var j = $('#mcq input[type="radio"]').length;
                if(j<4){
                            $('#add_mcq_option').show();
                        }
            });      

            var a=$('#maq input[type="checkbox"]').length;
            //adding maq options dynamically
            $('#add_maq_option').click(function(){
                a++;
                $('#maq').append('<div class="col-md-12 row" id="row'+a+'"><div class="icheck-success"><input type="checkbox" name="maq_points[]" id="checkbox'+a+'" value="'+a+'"><label for="checkbox'+a+'"></label></div><div class="col-md-8"><input class="{{ $errors->has('option_text2') ? 'is-invalid' : '' }}" name="option_text2['+(a-1)+']" id="option_text2" type="text" value="{{ old('option_text2[]', '') }}" required><label for="option_text2" placeholder="New Option" alt="option"></label></div><div class="col-md-1"><button id="'+a+'" class="btn btn-secondary remove2" alt="Delete this option"><i class="fas fa-trash"></i></button></div>@if($errors->has('option_text2'))<div class="invalid-feedback">{{ $errors->first('option_text2') }}</div>@endif<span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span></div>');
                               
                var b = $('#maq input[type="checkbox"]').length;
                // console.log(j)
                if(b>=4){
                        $('#add_maq_option').hide();
                    }
            });
            
            $('body').on('click','.remove2',function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                var b = $('#maq input[type="checkbox"]').length;
                if(b<4){
                            $('#add_maq_option').show();
                        }
            });
            // displaying subcategory based on selected category
            $("#category_id").change(function(){
                var selected_id = $(this).val();
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.subcategories.getspecificsubcategory') }}',
                    type: 'get',
                    data: {categoryId:selected_id},
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        // console.log(data)
                        $("#subcategory_id").empty();
                        $("#subcategory_id").append("<option value=''>Please select a subcategory</option>");
                        $.each(data, function (id, value) {
                            $("#subcategory_id").append("<option value='"+id+"'>"+value+"</option>");
                            });
                    }
                });
            });  

            // hiding option area
            // $("#option , #add_option").hide();

            $("#type").change(function() {
                var selected_type = $(this).val();
                if(selected_type == 'Multiple Choices') {
                    $("#maq , #add_maq_option").hide();
                    $(".option_text2").removeAttr('required');
                    $("#torf").remove();
                    // $('#mcq , #add_mcq_option').removeClass('mcq , add_mcq_option');
                    $("#mcq , #add_mcq_option").show();
                    $("#option1, #option_text").attr('required','true');
                }
                else if(selected_type == 'Multiple Answers') {
                    $("#mcq , #add_mcq_option").hide();
                    $("#option1, #option_text").removeAttr('required');
                    $("#torf").remove();
                    // $('#mcq , #add_mcq_option').addClass('mcq , add_mcq_option');
                    $("#maq , #add_maq_option").show();
                    $(".option_text2").attr('required','true');
                }
                else if(selected_type == 'True or False ') {
                    $("#torf").remove();
                    $("#maq , #add_maq_option").hide();
                    $(".option_text2").removeAttr('required');
                    $("#mcq , #add_mcq_option").hide();
                    $("#option1, #option_text").removeAttr('required');
                    $("#quest").append(`<div class="torf" id="torf">
                                        <div class="col-md-12 row">
                                            <div class="icheck-success">
                                                <input type="radio" name="points" id="option1" value="1" >
                                                <label for="option1">
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                            <input class="{{ $errors->has('torf') ? 'is-invalid' : '' }}" name="torf[]" id="torf" type="text" value="True" readonly>
                                            
                                            
                                            </div>
                                            @if($errors->has('torf'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('torf') }}
                                                </div>
                                            @endif
                                            <span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span>
                                        </div>

                                        <div class="col-md-12 row">
                                            <div class="icheck-success">
                                                <input type="radio" name="points" id="option2" value="2" >
                                                <label for="option2">
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                            <input class="{{ $errors->has('torf') ? 'is-invalid' : '' }}" name="torf[]" id="torf" type="text" value="False" readonly>
                                            
                                            </div>
                                            @if($errors->has('torf'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('torf') }}
                                                </div>
                                            @endif
                                            <span class="help-block">{{ trans('cruds.option.fields.option_text_helper') }}</span>
                                        </div>
                                    </div>`);
                }
                else {
                    $("#maq , #add_maq_option").hide();
                    $("#mcq , #add_mcq_option").hide();
                    $("#torf").remove();
                    // $('#mcq , #add_mcq_option').addClass('mcq , add_mcq_option');
                }
            });
            
        });
    </script>
@endsection

