<!DOCTYPE html>
<html>

<head>
    <style>
        .logo-wrapper {
	text-align: center;
	margin-bottom: -37px;
}

.logo {
	width: 100px;
	display: inline-block;
}

.navbar {
	li {
		display: inline-block;
		padding: 0 10px 10px;
	}
}

.half {
	width: 50%;
	display: block;
	float: left;
}

.right-navlist {
	padding-left: 60px;
}

.left-navlist {
	text-align: right;
	padding-right: 60px;
}
        </style>
</head>
<body>
<div class="container">
    <div class="navbar navbar-default">
		<div class="logo-wrapper">
			<div class="logo"><img class="logo" src="http://www.hsdtaxlaw.com/wp-content/uploads/2016/05/logo_placeholder.png" alt=""></div>
		</div>
		<div class="half">		
			<ul class="left-navlist">
				<li>Home</li>
				<li>About</li>
			</ul>
		</div>
		<div class="half">		
			<ul class="right-navlist">
				<li>Contact</li>
				<li>Profile</li>
				<li>Maps</li>
			</ul>
		</div>
    </div>
   
    <div class="jumbotron">
        <h1>Twitter Bootstrap 3.0</h1>
        <p class="lead">Starter template with CSS and JS included.</p>
        <p><a class="btn btn-lg btn-primary" href="#fork">Fork this fiddle</a></p>
      </div>
</div>
</body>
</html>