<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ trans('panel.site_title') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background: #0F2027;
                background: -webkit-linear-gradient(to left, #2C5364, #203A43, #0F2027);
                background: linear-gradient(to left, #2C5364, #203A43, #0F2027);
                color: rgba(255, 255, 255, 0.76);
                font-family: 'Georgia', 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .title > a {
                color: rgba(255, 255, 255, 0.76);
            }

            .links > a {
                color: rgba(255, 255, 255, 0.76);
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            /* BUTTONS */
            .btn {
            font-size: 1.8rem;
            padding: 1rem 0;
            width: 20rem;
            text-align: center;
            border: 0.1rem solid #636b6f;
            margin-bottom: 1rem;
            text-decoration: none;
            color: #56a5eb;
            background-color: white;
            }

            .btn:hover {
            cursor: pointer;
            box-shadow: 0 0.4rem 1.4rem 0 rgba(86, 185, 235, 0.5);
            transform: translateY(-0.1rem);
            transition: transform 150ms;
            }

            .btn[disabled]:hover {
            cursor: not-allowed;
            box-shadow: none;
            transform: none;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <h3>Quick Quiz</h3>
                    {{-- <a class="btn" href="{{ route('client.test') }}">Play</a> --}}
                </div>

            </div>
        </div>
    </body>
</html>
