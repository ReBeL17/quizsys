<style>
    .flex-container {
      display: flex;
      /* background-color: #f1f1f1; */
      /* padding: 0px 20px; */
    }
    
    .flex-container > div.sub-category {
        background: #ADA996;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to left, #EAEAEA, #DBDBDB, #F2F2F2, #ADA996);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to left, #EAEAEA, #DBDBDB, #F2F2F2, #ADA996); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
      
        color: rgb(0, 0, 0);
        width: 7.85em;
        /* min-height: 6em; */
        /* max-width: 8em; */
        height: 6em;
        margin: 10px;
        text-align: center;
        align-items: center;
        /* line-height: 3em; */
        justify-content: center;
        font-size: 25px;
        border-radius: 10px;
        cursor: pointer;
        display: flex;
        background-attachment: fixed;
        transition: all 0.3s ease-in;
        flex-wrap: wrap;
    }

    .flex-container > div.sub-category:hover {
        /* opacity: 0.7; */
        box-shadow: 0 0.5rem 0.6rem 0 rgb(170, 168, 159);
        transform: translateY(-0.1rem);
        transition: transform 150ms;
    }

    .showall{
        color:rgb(1, 30, 41); 
        margin-top: -2.2em; 
        margin-bottom: 0em; 
        background-color:#ADA996; 
        border-radius:3px; 
        padding:3px;
        cursor: pointer;
    }
    .showall:hover{
        background-color:rgb(168, 157, 99); 
        text-decoration: none;
    }
    .performance{
        display: flex;
    }
    .user-activity{
        /* background: rgb(179, 178, 175); */
        background: #fff;
        opacity: 0.8;
        color: rgb(0, 0, 0);
        width: 100%;
        height: 6em;
        margin-bottom: 10px;
        text-align: center;
        align-items: center;
        justify-content: center;
        font-size: 25px;
        border-radius: 10px;
        display: flex;
        background-attachment: fixed;
        flex-wrap: wrap;
    }
    </style>