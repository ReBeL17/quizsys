<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Congrats!</title>
    <link rel="stylesheet" href="{{ asset('/css/quiz/app.css')}}" />
  </head>
  <body>
    <div class="container">
      <div id="end" class="flex-center flex-column">
        <h3> Final Score </h3>
        <h1 id="finalScore"></h1>
        
        <a class="btn" href="{{route('client.startQuiz', $id)}}">Play Again</a>
        <a class="btn" href="{{route('client.home')}}">Go Home</a>
      </div>
    </div>
    <script src="{{ asset('/js/end.js')}}"></script>
  </body>
</html>
