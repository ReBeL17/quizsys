@extends('layouts.client')

@include('partials.client.head')


@section('content')
<div class="container">    
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="col-lg-12 row performance" style="margin: 15px 0px;">
                <div class="col-lg-3 col-6">
                      <div class="user-activity">{{$total_score = auth()->user()->userResults()->sum('total_points')}}  <br>  Total Score</div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                        <div class="user-activity">{{ round(($total_score / ($attempted * 5))*10, 2)}} / 10 <br>  Average Score</div>  
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                        <div class="user-activity">{{auth()->user()->userResults()->count()}} <br> Quizes Played</div>  
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <div class="user-activity">{{$attempted}} <br> Questions Faced</div>  
                </div>
                <!-- ./col -->
              </div>
        <hr style="border:1px solid #2C5364;">

            @foreach($categories as $key=>$category)
            <div class="col-md-12" style="padding: 15px 15px;">
                @foreach($category as $subcategory)
                    <h3>{{$subcategory->category->name}}</h3>
                    @if ($categoy[$key]->last_page > 1)
                        <a href="{{url('showall',$subcategory->category->id)}}" class="showall float-right" > see more </a>
                    @endif
               
                    @break
                    @endforeach
                <div class="row flex-container">
                    @foreach($category as $subcategory)
                        <div class="sub-category" onclick="getQuestions({{$subcategory->id}})">{{$subcategory->name}}</div>
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>

        <div class="col-md-2">
            <div class="card" style="padding: 10px;">
            <h6> Total Participants : <strong> {{$total_users}} </strong></h6>
            <hr style="border:1px solid #2C5364;">
            <h6>Top 10 Scorers:</h6>
                @foreach($results as $result)
                    <small class="pull-left" style="margin:5px 0px; border:1px solid #2C5364; border-radius:5px; padding:2px; font-size:15px;">{{$result->user->name}} : <strong>{{$result->total}}</strong> </small>
                @endforeach
            </div>
        </div>
    </div>
</div>

@include('partials.client.footer')
@endsection