@extends('layouts.client')
@include('partials.client.head')

@section('content')
<div class="container">
   <p> Popular categories under <strong>{{$category->name}}</strong> </p>
        <hr style="border:1px solid #203A43;">
        <div class="row flex-container">
            @foreach ($category->subcategories as $subcategory)
                <div class="sub-category" style="min-width: 10em; min-height:9em;" onclick="getQuestions({{$subcategory->id}})">{{$subcategory->name}}</div>
            @endforeach
        </div>
</div>

@include('partials.client.footer')
@endsection