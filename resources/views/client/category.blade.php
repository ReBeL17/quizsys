<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Quick Quiz - Play</title>
    <link rel="stylesheet" href="{{ asset('css/quiz/category.css')}}" />
    {{-- <link rel="stylesheet" href="{{ asset('css/app.css')}}" /> --}}
  </head>
  <body>
    <div class="container">
        @foreach($categories as $category)
            <div class="bg1" onclick="getQuestions({{$category->id}})">
            <h2>{{$category->name}}</h2>
            {{-- <p>Goals Completed</p> --}}
            </div>
        @endforeach
    {{-- <div class="bg1">
      <h2><i class="fas fa-battery-three-quarters"></i></h2>
      <p>Respiration</p>
    </div>
    <div class="bg2">
      <h2><i class="fas fa-running"></i></h2>
      <p>Miles</p>
    </div>
    <div class="bg1">
      <h2>36 &deg;</h2>
      <p>Temperature</p>
    </div>
    <div class="bg1">
      <h2><i class="fas fa-bed"></i></h2>
      <p>Sleep Keep</p>
    </div>
    <div class="bg2">
      <h2>98 <span>bpm</span></h2>
      <p>Heart Rate</p>
    </div>
    <div class="bg1">
      <h2>170 <span>lbs</span></h2>
      <p>Weight</p>
    </div>
    <div class="bg1">
      <h2>28 <span>%</span></h2>
      <p>Fat Percentage</p>
    </div>
    <div class="bg2">
      <h2>118 <span>mgdl</span></h2>
      <p>Blood Glucose</p>
    </div>
    <div class="bg2">
      <h2>680 <span>kcal</span></h2>
      <p>AVG Consumption</p>
    </div>
    <div class="bg2">
      <h2><i class="fas fa-dumbbell"></i></h2>
      <p>Workouts</p>
    </div>
    <div class="bg2">
      <h2>85 <span>%</span></h2>
      <p>Body Hydration</p>
    </div> --}}
  </div>
  <script>
    function getQuestions(id){
        const category_id = id;
        return window.location.assign("/start_quiz/"+ category_id);
      }
  </script>
  </body>
</html>