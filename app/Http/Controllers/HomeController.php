<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Result;
use App\User;
use App\Question_Result;
use App\Category;
use App\Subcategory;
use Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_users = User::all()->count();
        $results = Result::selectRaw("sum(total_points) as total,user_id")->with('user')
                    ->orderBy('total','desc')
                    ->groupBy("user_id")
                    ->paginate(10);
    
       $total_results = Auth::user()->userResults()->with('questions')->get();
       $questions_attempted = [];
       foreach($total_results as $index=>$total){
        foreach($total->questions as $i=>$t){
                $questions_attempted[$index][$i] = $t->pivot->question_id;
            }
        }
        
        $attempted=0;
        foreach($questions_attempted as $question){
            $attempted+=count($question);
        }

        // $categories = Category::with(['subcategories' => function ($query) use ($id) {
        //                             $query->where('category_id', $id)->latest()->paginate(5);
        //                         }])
        //                         ->whereHas('subcategories')
        //                         ->latest()
        //                         ->limit(5)
        //                         ->get();

        $categories = Category::whereHas('subcategories')
                                ->latest()
                                ->get();
        foreach($categories as $key=>$category) {
            $categories[$key] = Subcategory::where('category_id',$category->id)
                                    ->with('category')
                                    ->latest()
                                    ->paginate(5);                     
        }
        // $categories->getCollection()->transform(function ($item) {
        //     return $item->subcategories->split(5);
        // });
        // dd($categories);
        $categoy = json_encode($categories);
                        $categoy = json_decode($categoy);
                        // dd($categoy[1]->last_page);
        return view('client.home', compact('total_users', 'results','attempted','categories','categoy'));
    }

    public function redirect()
    {
        if (auth()->user()->is_admin) {
            return redirect()->route('admin.home')->with('status', session('status'));
        }

        return redirect()->route('client.home')->with('status', session('status'));
    }

    public function showAll($id) {
        $category = Category::with('subcategories')->find($id);
        return view('client.subcategory',compact('category'));
    }
}
