<?php

namespace App\Http\Controllers\Admin;
use App\User;
use App\Category;
use App\Question;
use App\Result;

class HomeController
{
    public function index()
    {
        $total_users = User::count();
        $total_categories = Category::count();
        $total_questions = Question::count();
        $total_games = Result::count();

        $results = Result::selectRaw("sum(total_points) as total,user_id")->with('user')
                    ->orderBy('total','desc')
                    ->groupBy("user_id")
                    ->limit(10)
                    ->get();

        return view('home', compact('total_users','total_categories','total_questions','total_games','results'));
    }
}
