<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Subcategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyQuestionRequest;
use App\Http\Requests\StoreQuestionRequest;
use App\Http\Requests\UpdateQuestionRequest;
use App\Question;
use App\Option;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class QuestionsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('question_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $questions = Question::with('subcategory')->get();

        return view('admin.questions.index', compact('questions'));
    }

    public function create()
    {
        abort_if(Gate::denies('question_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect').' a category', '');

        return view('admin.questions.create', compact('categories'));
    }

    public function store(StoreQuestionRequest $request)
    {
        // if($request->points){
        // dd($request->points);
        // }
        // dd($request->all());
        $data1 = [
            'question_text' => $request->question_text,
            'subcategory_id' => $request->subcategory_id,
            'type' => $request->type,
        ];
        $question = Question::create($data1);

        //inserting data into options table.
        if($request->type == 'Multiple Choices'){
            $options = $request->option_text;
            $point = $request->points;
            foreach($options as $key => $option){                
            $data2 = [
                'option_text' => $option,
                'points' => (($key+1)==$point)?1:0,
                'question_id' => $question->id,
            ];
                Option::create($data2);
            }
        }
        elseif($request->type == 'Multiple Answers'){
            $options = $request->option_text2;
            $point = $request->maq_points;
            foreach($options as $key => $option){                
                $data2 = [
                    'option_text' => $option,                                                      
                    'question_id' => $question->id,
                ];   
                foreach($point as $p) { 
                    if($key+1 == $p) {
                        $data2['points'] = 1;
                            break;
                    } else {
                        $data2['points'] = 0;
                    }
                }
                Option::create($data2);
            }
        }
        elseif($request->type == 'True or False'){
            $options = $request->torf;
            $point = $request->points;
            foreach($options as $key => $option){                
            $data2 = [
                'option_text' => $option,
                'points' => (($key+1)==$point)?1:0,
                'question_id' => $question->id,
            ];
                Option::create($data2);
            }
        }
        return redirect()->route('admin.questions.index');
    }

    public function edit(Question $question)
    {
        abort_if(Gate::denies('question_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect').' a category', '');

        $question->load('subcategory','questionOptions');
        if($question->subcategory){
            $category_id = $question->subcategory->category->id;
            
            $subcategories = Subcategory::where('category_id',$category_id)->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }
        return view('admin.questions.edit', compact('categories', 'question','subcategories'));
    }

    public function update(UpdateQuestionRequest $request, Question $question)
    {
    //    dd($request->all());
       $data1 = [
                    'question_text' => $request->question_text,
                    'subcategory_id' => $request->subcategory_id,
                    'type' => $request->type,
                ];
        $question->update($data1);
                // dd($question);
        //inserting data into options table.
        if($request->type == 'Multiple Choices') {
            $options = $request->option_text;
            $point = $request->points;
            $prevOptions = $question->questionOptions->pluck('id');
            
            if(count($options) == count($prevOptions)) {
                foreach($options as $key => $option) {
                
                    $data2 = [
                        'option_text' => $option,
                        'points' => (($key+1)==$point)?1:0,
                        'question_id' => $question->id,
                    ];

                    Option::updateOrInsert(
                        ['question_id'=>$question->id, 'id'=>$prevOptions[$key] ],
                        $data2
                    );
                }
            } else {
                Option::where('question_id',$question->id)->delete();
                foreach($options as $key => $option) {
                
                    $data2 = [
                        'option_text' => $option,
                        'points' => (($key+1)==$point)?1:0,
                        'question_id' => $question->id,
                    ];
                    Option::create($data2);
                }
            }
        }
        elseif($request->type == 'Multiple Answers') {
            $options = $request->option_text2;
            $point = $request->maq_points;
            $prevOptions = $question->questionOptions->pluck('id');
            
            if(count($options) == count($prevOptions)) {
                foreach($options as $key => $option) {
                $data2 = [
                    'option_text' => $option,                                                      
                    'question_id' => $question->id,
                ];   
                foreach($point as $p) { 
                    if($key+1 == $p) {
                        $data2['points'] = 1;
                            break;
                    } else {
                        $data2['points'] = 0;
                    }
                }

                    Option::updateOrInsert(
                        ['question_id'=>$question->id, 'id'=>$prevOptions[$key] ],
                        $data2
                    );
                }
            } else {
                Option::where('question_id',$question->id)->delete();
                foreach($options as $key => $option) {
                
                    $data2 = [
                        'option_text' => $option,                                                      
                        'question_id' => $question->id,
                    ];   
                    foreach($point as $p) { 
                        if($key+1 == $p) {
                            $data2['points'] = 1;
                                break;
                        } else {
                            $data2['points'] = 0;
                        }
                    }
                    Option::create($data2);
                }
            }
        }
        if($request->type == 'True or False') {
            $options = $request->torf;
            $point = $request->points;
            $prevOptions = $question->questionOptions->pluck('id');
            
            // if(count($options) == count($prevOptions)) {
                foreach($options as $key => $option) {
                
                    $data2 = [
                        'option_text' => $option,
                        'points' => (($key+1)==$point)?1:0,
                        'question_id' => $question->id,
                    ];

                    Option::updateOrInsert(
                        ['question_id'=>$question->id, 'id'=>$prevOptions[$key] ],
                        $data2
                    );
                // }
            } 
        }
        return redirect()->route('admin.questions.index');
    }

    public function show(Question $question)
    {
        abort_if(Gate::denies('question_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $question->load('subcategory');

        return view('admin.questions.show', compact('question'));
    }

    public function destroy(Question $question)
    {
        abort_if(Gate::denies('question_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $question->delete();

        return back();
    }

    public function massDestroy(MassDestroyQuestionRequest $request)
    {
        Question::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
