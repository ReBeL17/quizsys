<?php

namespace App\Http\Controllers;

use App\Category;
use App\Question;
use App\Http\Requests\StoreTestRequest;
use Illuminate\Http\Request;
use App\Option;

class TestsController extends Controller
{
    public function index()
    {
        $categories = Category::inRandomOrder()->get();
        return view('client.category', compact('categories'));
    }

    public function startQuiz($id)
    {
        return view('client.test', compact('id'));
    }

    public function getQuestions($category_id)
    {
        $questions = Question::where('subcategory_id', $category_id)->with(['questionOptions' => function ($query) {
                        $query->inRandomOrder();
                     }])
        ->whereHas('questionOptions')
        ->get();
        // dd($questions);
        return $questions;
    }

    // public function store(StoreTestRequest $request)
    // {
    //     $options = Option::find(array_values($request->input('questions')));

    //     $result = auth()->user()->userResults()->create([
    //         'total_points' => $options->sum('points')
    //     ]);

    //     $questions = $options->mapWithKeys(function ($option) {
    //         return [$option->question_id => [
    //                     'option_id' => $option->id,
    //                     'points' => $option->points
    //                 ]
    //             ];
    //         })->toArray();

    //     $result->questions()->sync($questions);

    //     return redirect()->route('client.results.show', $result->id);
    // }

    public function store(Request $request)
    {
        
        $result = auth()->user()->userResults()->create([
            'total_points' => $request->score
        ]);

        // dd($request->input('selected_choices', []));
        $choices = $request->input('selected_choices', []);
            // dd($choices);
        foreach($request->input('answered_question', []) as $key=>$question){

            $result->questions()->sync([$question => ['option_id' => $choices[$key] ] ]);
        }
        // $result->questions()->sync([$request->input('answered_question', []) => ['option_id' => $request->input('selected_choices', [])] ]);
        // $result->questions()->sync([ $result => ['option_id' => $request->input('selected_choices', [])] ]);
    }
}
