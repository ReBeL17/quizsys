<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');

            $table->longText('question_text');
            $table->longText('question_hint')->nullable();
            $table->string('image')->nullable();
            $table->longText('answer_explanation')->nullable();
            $table->timestamps();

            $table->softDeletes();
        });
    }
}
